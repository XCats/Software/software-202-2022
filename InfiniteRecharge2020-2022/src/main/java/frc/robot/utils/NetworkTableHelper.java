package frc.robot.utils;

import java.util.Map;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;

public class NetworkTableHelper {

    public static void putNetworkTableData(NetworkTable networkTable, Map<String, Object> dataMap) {
        putData(dataMap, networkTable, null);
    }

    public static void putTabbedNetworkTableData(String tabName, Map<String, Object> dataMap) {
        ShuffleboardTab tab = Shuffleboard.getTab(tabName);
        putData(dataMap, null, tab);
    }

    /**
     * 
     * @param dataMap
     * @param networkTable
     * @param tab
     * Loop through all items in data map and set values in given network table or shuffleboard tab
     */
    private static void putData(Map<String, Object> dataMap, NetworkTable networkTable, 
        ShuffleboardTab tab) {

        NetworkTableEntry entry;

        for(String key : dataMap.keySet()) {
            Object value = dataMap.get(key);
            if(networkTable != null) {
                entry = networkTable.getEntry(key);
            }
            else {
                entry = tab.add(key, value).getEntry();
            }

            if (value instanceof Boolean) {
                entry.setBoolean((Boolean) value);
            } else if (value instanceof Number) {
                entry.setNumber((Number) value);
            } else if (value instanceof String) {
                entry.setString((String) value);
            } else if (value instanceof byte[]) {
                entry.setRaw((byte[]) value);
            } else if (value instanceof boolean[]) {
                entry.setBooleanArray((boolean[]) value);
            } else if (value instanceof double[]) {
                entry.setDoubleArray((double[]) value);
            } else if (value instanceof Number[]) {
                entry.setNumberArray((Number[]) value);
            } else if (value instanceof String[]) {
                entry.setStringArray((String[]) value);
            } 
        }
    }
}