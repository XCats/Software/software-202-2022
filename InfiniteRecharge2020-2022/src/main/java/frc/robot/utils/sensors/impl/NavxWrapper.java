package frc.robot.utils.sensors.impl;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.SPI;

public class NavxWrapper extends XBaseInu {
    private final AHRS mAhrs;

    public NavxWrapper() {
        this(GyroDirection.Yaw);
    }

    public NavxWrapper(GyroDirection gyroDirection) {
        super("Navx", gyroDirection);
        mAhrs = new AHRS(SPI.Port.kMXP);
    }


    @Override
    public double getRawPitch() {
        return mAhrs.getPitch();
    }

    @Override
    public double getRawRoll() {
        return mAhrs.getRoll();
    }

    @Override
    public double getRawYaw() {
        return mAhrs.getYaw();
    }
}
