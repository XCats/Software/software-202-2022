package frc.robot.utils.sensors.impl;

import java.util.HashMap;
import java.util.Map;

import com.revrobotics.ColorMatch;
import com.revrobotics.ColorMatchResult;
import com.revrobotics.ColorSensorV3;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.util.Color;
import frc.robot.Constants;
import frc.robot.utils.NetworkTableHelper;

public class ColorSensorWrapper {

    private final ColorSensorV3 mColorSensor;
    private final ColorMatch mColorMatcher;
    private final NetworkTable mColorTable;
    private Color mDetectedColor;
    private Color mMatchColor;
    private ColorMatchResult mMatchResult;
    private boolean mIsMatch;

    public ColorSensorWrapper(ColorSensorV3 colorSensor, ColorMatch colorMatcher) {
        mColorSensor = colorSensor;
        mColorMatcher = colorMatcher;
//        mColorMatcher.addColorMatch(Constants.BLUE_CONTROL_PANEL_COLOR.getValue());
//        mColorMatcher.addColorMatch(Constants.GREEN_CONTROL_PANEL_COLOR.getValue());
//        mColorMatcher.addColorMatch(Constants.RED_CONTROL_PANEL_COLOR.getValue());
//        mColorMatcher.addColorMatch(Constants.YELLOW_CONTROL_PANEL_COLOR.getValue());
        mColorTable = NetworkTableInstance.getDefault().getTable("RobotData").getSubTable("Color");
    }

    public Color getDetectedColor() {
        mDetectedColor = mColorSensor.getColor();
        return mDetectedColor;
    }

    public ColorMatchResult getMatchedDetectedColor() {
        return mColorMatcher.matchClosestColor(mDetectedColor);
    }

    public Color getMatchColor() {
        return mMatchColor;
    }

    public void setMatchColor(String matchColor) {

        if (matchColor.length() > 0) {
            switch (matchColor.charAt(0)) {
            // Blue case code
            case 'B':
                setMatchColor(Constants.BLUE_CONTROL_PANEL_COLOR.getValue());
                break;
            // Green case code
            case 'G':
                setMatchColor(Constants.GREEN_CONTROL_PANEL_COLOR.getValue());
                break;
            // Red case code
            case 'R':
                setMatchColor(Constants.RED_CONTROL_PANEL_COLOR.getValue());
                break;
            // Yellow case code
            case 'Y':
                setMatchColor(Constants.YELLOW_CONTROL_PANEL_COLOR.getValue());
                break;
            default:
                // This is corrupt data
                break;
            }
        }
    }

    public void setMatchColor(Color matchColor) {
        mMatchColor = matchColor;   
    }

    public void reset() {
        System.out.println("ColorSensorWrapper -- Color values reset");
        mDetectedColor = null;
        mMatchColor = null;
        mMatchResult = null;
        mIsMatch = false;
    }

    public boolean isMatch() {

        if (mMatchColor != null) {
            Color detectedColor = getDetectedColor();
            mMatchResult = mColorMatcher.matchClosestColor(detectedColor);
            mIsMatch = mMatchColor == mMatchResult.color;
        }
        else {
            mIsMatch = false;
        }
        putNetworkTableData();
        return mIsMatch;
    }

    public boolean isMatch(Color color) {
        setMatchColor(color);
        return isMatch();
    }

    public void putNetworkTableData() {
        Map<String, Object> dataMap = new HashMap<String, Object>() {
            {
                put("Driver Station Color", getColorString(mMatchColor));
                put("Detected Color", getColorString(mDetectedColor));
                put("Matched Color", getColorString(mMatchResult.color));
                put("Confidence", mMatchResult.confidence);
                put("IsMatch", mIsMatch);
            }
        };

        NetworkTableHelper.putNetworkTableData(mColorTable, dataMap);
    }

    public void setConfidenceLevel(double confidenceLevel) {
        mColorMatcher.setConfidenceThreshold(confidenceLevel);
    }

    public void addColorToMatch(Color color) {
        mColorMatcher.addColorMatch(color);
    }

    private String getColorString(Color color) {
        return String.format("Red: %2f Green: %2f Blue: %2f", 
            color.red, color.green, color.blue);
    }
}