package frc.robot.utils.controllers;

import edu.wpi.first.wpilibj.SpeedController;
import java.util.function.*;
public interface XSpeedController extends SpeedController {
    void setmotionMagicSetpoint(double setPoint);

    void setVelocitySetpoint(double setPoint);

    default Consumer<Double> getPConsumer() {
        return new Consumer<Double>(){
            @Override
            public void accept(Double arg0) {
                setP(arg0);
            }
        };
    }

    default Consumer<Double> getIConsumer() {
        return new Consumer<Double>(){
            @Override
            public void accept(Double arg0) {
                setI(arg0);
            }
        };
    }

    default Consumer<Double> getDConsumer() {
        return new Consumer<Double>(){
            @Override
            public void accept(Double arg0) {
                setD(arg0);
            }
        };
    }

    default Consumer<Double> getFConsumer() {
        return new Consumer<Double>(){
            @Override
            public void accept(Double arg0) {
                setFF(arg0);
            }
        };
    }


    void setP(double p);

    void setI(double i);

    void setD(double d);

    void setIz(double Iz);

    void setFF(double ff);

    void setMaxPIDOutput(double maxOutput);

    void setMinPIDOutput(double minOutput);

    void setMaxVelocity(double maxVelocity);

    void setMinVelocity(double minVelocity);

    void setMaxAcceleration(double maxAcceleration);

    void setAllowedClosedLoopError(double allowedClosedLoopError);

    boolean getAtSetpoint();

    boolean getAtVelocitySetpoint(double percentError);
    
}