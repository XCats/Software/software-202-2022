package frc.robot.auto;

import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.auto.complex_commands.AutoCenterMode;
import frc.robot.auto.complex_commands.AutoLeftMode;
import frc.robot.auto.complex_commands.AutoRightMode;
import frc.robot.processors.Limelight;
import frc.robot.subsystems.CarouselSubsystem;
import frc.robot.subsystems.ShooterSubsystem;
import frc.robot.subsystems.TurretSubsystem;
import frc.robot.subsystems.drive.TankDriveBase;

public class AutoSelector {
    private final SendableChooser<Command> mAutoChooser = new SendableChooser<Command>();

    public AutoSelector(TankDriveBase driveBase, TurretSubsystem turret, ShooterSubsystem shooterSubSystem, Limelight limelight) {
        mAutoChooser.addOption("left drive and shoot", new AutoLeftMode(driveBase, turret, shooterSubSystem, limelight));
        mAutoChooser.addOption("center shoot and drive", new AutoCenterMode(driveBase, turret, shooterSubSystem, limelight));
        mAutoChooser.addOption("Right (Trench) shoot and drive", new AutoRightMode(driveBase, turret, shooterSubSystem, limelight));
        SmartDashboard.putData("Auto Mode", mAutoChooser);
    }

    public Command getSelectedCommand() {
        return mAutoChooser.getSelected();
    }



}