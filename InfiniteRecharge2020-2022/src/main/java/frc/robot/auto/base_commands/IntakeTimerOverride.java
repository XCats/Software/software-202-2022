/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.auto.base_commands;

import frc.robot.commands.acquisition.ArmDownThenIntakeCommand;
import frc.robot.subsystems.AcquisitionArmSubsystem;
import frc.robot.subsystems.AcquisitionRollersSubsystem;
import frc.robot.subsystems.CarouselSubsystem;
import frc.robot.utils.XTimerOverrideCommand;

/**
 * An example command that uses an example subsystem.
 */
public class IntakeTimerOverride extends XTimerOverrideCommand<ArmDownThenIntakeCommand> {
  @SuppressWarnings({ "PMD.UnusedPrivateField", "PMD.SingularField" })

  public IntakeTimerOverride(AcquisitionArmSubsystem acquisitionArmSubsystem, CarouselSubsystem carouselSubsystem,
      AcquisitionRollersSubsystem acquisitionRollersSubsystem, double time) {
    super(time, new ArmDownThenIntakeCommand(acquisitionArmSubsystem, carouselSubsystem, acquisitionRollersSubsystem));

  }

}
