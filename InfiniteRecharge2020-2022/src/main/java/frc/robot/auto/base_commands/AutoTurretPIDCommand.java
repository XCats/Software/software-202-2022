package frc.robot.auto.base_commands;


import frc.robot.Constants;
import frc.robot.commands.turret.TurretPIDCommand;
import frc.robot.processors.Limelight;
import frc.robot.subsystems.TurretSubsystem;
import frc.robot.utils.XTimerOverrideCommand;

public class AutoTurretPIDCommand extends XTimerOverrideCommand<TurretPIDCommand> {

  private double mSpeed;
  private TurretSubsystem mTurret;
  private Limelight mLimelight;
  private double mTx;

  public enum Mode {
    LEFT_SIDE(1), RIGHT_SIDE(-1), Center(0);

    public final int mSpeed;

    private Mode(int speed) {
      this.mSpeed = speed;
    }
  }

  private Mode mMode;

  // mpk - adding these as it seems Constants.java is not read
  private final static double TURRET_SPEED = 0.2; // 0.1 works well w/ .003
  private final static double TURRET_KP = 0.006;
  private final static double TURRET_ALLOWABLE_ERROR = 0.5; // in degrees

  public AutoTurretPIDCommand(TurretSubsystem turret, Limelight limelight, Mode mode) {
    super(Constants.DEFAULT_TURRET_TIMER_OVERRDIE, new TurretPIDCommand(turret, limelight));
    this.mTurret = turret;
    this.mLimelight = limelight;
    this.mMode = mode;
  }

  @Override
  public void execute() {
    if (mLimelight.seesTarget()) {
      mTx = mLimelight.getRawYawError(); // L/R offset from target
      System.out.print(" mTx: " + mTx);
      if (mTx > TURRET_ALLOWABLE_ERROR) {
        // mSpeed = -Constants.TURRET_SPEED.getValue() + (mTx *
        // Constants.TURRET_KP.getValue());
        mSpeed = -(TURRET_SPEED + (mTx * TURRET_KP));
      } else if (mTx < -TURRET_ALLOWABLE_ERROR) {
        // mSpeed = Constants.TURRET_SPEED.getValue() + (mTx *
        // Constants.TURRET_KP.getValue());
        mSpeed = (TURRET_SPEED + (-mTx * TURRET_KP));
      } else
        mSpeed = 0.0;
      System.out.println("  ::  mSpeed: " + mSpeed);
    }

    // System.out.print(" " + mSpeed);
    mTurret.setSpeed(mMode.mSpeed); // right -> CW ; left -> CCW

  }

}
