package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.util.sendable.SendableBuilder;
import frc.robot.Constants;
import frc.robot.utils.controllers.XSpeedController;

public class ClimbingSubsystem extends XPneumaticSubsystem {

    //Add controllers
    XSpeedController m_ClimberMotor;

    
    public ClimbingSubsystem(DoubleSolenoid climber, XSpeedController winch) {
        super("Climber", climber, true);
        m_ClimberMotor = winch;

    }

    @Override
	public void initSendable(SendableBuilder builder) {
        super.initSendable(builder);
		builder.addDoubleProperty(getName() + " motor", ClimbingSubsystem.this::getSpeed, null);
	}

    public double getSpeed() {
        return m_ClimberMotor.get();
    }

    public void ClimbingMotorOn() {
       //when the robot is going to climb turn the motor on 
       m_ClimberMotor.set(Constants.CLIMBER_SPEED.getValue());
       System.out.println("climber motor is on at speed " + Constants.CLIMBER_SPEED.getValue());
       retract();
    }

    public void ClimbingMotorOff() {
        //when the robot is done climbing turn the motor off
        m_ClimberMotor.stopMotor();
    }

    @Override
    public void periodic() {
        // This method will be called once per scheduler run

    }
}
