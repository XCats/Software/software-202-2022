/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.util.sendable.SendableBuilder;

public class AcquisitionArmSubsystem extends XPneumaticSubsystem {
    /**
     * Creates a new Aquisition Subsystem.
     */
    public AcquisitionArmSubsystem(DoubleSolenoid arm) {
        super("Acquisition Arm", arm, true);
    }

    @Override
	public void initSendable(SendableBuilder builder) {
        super.initSendable(builder);
	}

    // public void armUp() {
    //     mArmValue = Value.kReverse;
    //     mArm.set(mArmValue);

    // }

    // public Value getArmValue() {
    //     return mArmValue;
    // }

    // private String getArmValueString() {
    //     return mArmValue.getClass().getName();
    // }

  
    @Override
    public void periodic() {
        // This method will be called once per scheduler run
    }

}
