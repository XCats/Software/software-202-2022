/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.drive;

import frc.robot.subsystems.drive.TankDriveBase;
import frc.robot.utils.triggers.DriveJoystickSmoother;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.CommandBase;

/**
 * An example command that uses an example subsystem.
 */
public class DefaultDriveBaseCommand extends CommandBase {
  @SuppressWarnings({"PMD.UnusedPrivateField", "PMD.SingularField"})
  private final TankDriveBase m_DriveSubsystem;
    private Joystick m_leftJoystick;
    private Joystick m_rightJoystick;
  /**
   * Creates a new ExampleCommand.
   *
   * @param subsystem The subsystem used by this command.
   */
  public DefaultDriveBaseCommand(TankDriveBase subsystem, Joystick leftJoystick, Joystick rightJoystick) {
    m_leftJoystick = leftJoystick;
    m_rightJoystick = rightJoystick;
    m_DriveSubsystem = subsystem;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(subsystem);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    // //Get input from joysticks
    // "Mode" 2
    // m_DriveSubsystem.setRightSpeed(DriveJoystickSmoother.smooth(m_rightJoystick.getY()));
    // m_DriveSubsystem.setLeftSpeed(DriveJoystickSmoother.smooth(m_leftJoystick.getY()));

    // "Mode" 3
    DriveJoystickSmoother.extrasmooth(m_rightJoystick.getY(), false);
    DriveJoystickSmoother.extrasmooth(m_leftJoystick.getY(), true);
    m_DriveSubsystem.setRightSpeed(DriveJoystickSmoother.getRight());
    m_DriveSubsystem.setLeftSpeed(DriveJoystickSmoother.getLeft());
    //System.out.println("right speed" + m_rightJoystick.getY());
    //System.out.println("left speed" + m_leftJoystick.getY());
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
