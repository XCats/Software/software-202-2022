package frc.robot.commands.turret;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.processors.Limelight;
import frc.robot.subsystems.TurretSubsystem;

public class TurretPIDCommand extends CommandBase {

    private double mSpeed;
    private TurretSubsystem mTurret;
    private Limelight mLimelight;
    private double mTx;
    // mpk - adding these as it seems Constants.java is not read
    // private final static double TURRET_SPEED = 0.1; // 0.1 works well w/ .003
    // private final static double TURRET_KP = 0.006;
    // private final static double TURRET_ALLOWABLE_ERROR = 0.5; // in degrees

    public TurretPIDCommand(TurretSubsystem turret, Limelight limelight) {
      super();
      this.mTurret = turret;
      this.mLimelight = limelight;
    
      this.addRequirements(mTurret, mLimelight);
  }
     // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    System.out.println("Testing TurretPID");
    if (mLimelight.seesTarget()) {
      System.out.println("    Constants.TURRET_SPEED = " + Constants.TURRET_SPEED.getValue());
      System.out.println("    Constants.TURRET_KP = " + Constants.TURRET_KP.getValue());
      System.out.println("    Constants.TURRET_ALLOWABLE_ERROR = " + Constants.TURRET_ALLOWABLE_ERROR.getValue());
      System.out.println("    X offsets (degrees) :: Turret speed ");
      System.out.println("  Starting encoder RotDist value = " + mTurret.getEncoderRotDist() 
        + "  and ticks = " + mTurret.getEncoderDistTicks());
      System.out.println("    LL2 getDist() = " + mLimelight.getDistance());
    }
    mSpeed = 0.0;
    mTx = mLimelight.getRawYawError();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (mLimelight.seesTarget()) {
      mTx = mLimelight.getRawYawError(); // L/R offset from target
      double mTurretAllowableError = Constants.TURRET_ALLOWABLE_ERROR.getValue();
      System.out.print(" mTx: " + mTx);
        if (mTx > mTurretAllowableError) {
            mSpeed = -(Constants.TURRET_SPEED.getValue() + (mTx * Constants.TURRET_KP.getValue()));
            //mSpeed = -(TURRET_SPEED + (mTx * TURRET_KP));
        } else if (mTx < -mTurretAllowableError) {
            mSpeed = (Constants.TURRET_SPEED.getValue() + (-mTx * Constants.TURRET_KP.getValue()));
            //mSpeed = (TURRET_SPEED + (-mTx * TURRET_KP));
        } else mSpeed = 0.0;
        System.out.println("  ::  mSpeed: " + mSpeed);
    }

    //System.out.print("  " + mSpeed);
    mTurret.setSpeed(mSpeed); // right -> CW ; left -> CCW

  }


  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
      mTurret.setSpeed(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    boolean isFinished = false;
    double mError = Constants.TURRET_ALLOWABLE_ERROR.getValue();
    //mError = TURRET_ALLOWABLE_ERROR;  // TODO: fix when Constants values are init'd
    if (Math.abs(mLimelight.getRawYawError()) < mError) {
      isFinished = true;
      if (mLimelight.seesTarget()) {
        System.out.println("  Now pointing at target, within error = " + mError);
        System.out.println("  Ending encoder value = " + mTurret.getEncoderRotDist());
      }
      else 
        System.out.println("  No target seen. (done) ");
        System.out.println("  Ending encoder RotDist value = " + mTurret.getEncoderRotDist() 
        + "  and ticks = " + mTurret.getEncoderDistTicks());
        System.out.println("    LL2 getDist() = " + mLimelight.getDistance());
    } 
    
    return isFinished;
  }
}