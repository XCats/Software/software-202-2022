package frc.robot.commands.controlPanel;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ControlPanelSubsystem;

public class ControlPanelMatchColorCommand extends CommandBase {
    @SuppressWarnings({"PMD.UnusedPrivateField", "PMD.SingularField"})
    
    private final ControlPanelSubsystem mControlPanelSubsystem;
    private boolean mIsFinished;

    public ControlPanelMatchColorCommand(ControlPanelSubsystem controlPanelSubsystem) {
        super();
        mControlPanelSubsystem = controlPanelSubsystem;
        addRequirements(mControlPanelSubsystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        System.out.println("ControlPanelMatchColorCommand -- Initialized");
        mIsFinished = false;
        mControlPanelSubsystem.resetColorValues();
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        
        if(mControlPanelSubsystem.getState()) {          
            mControlPanelSubsystem.retract();
            System.out.println("ControlPanelMatchColorCommand -- Arm extended");
        }

        if (mControlPanelSubsystem.isMatch()) {
            System.out.println("ControlPanelMatchColorCommand -- Matched the color");
            mIsFinished = true;
        } else {
            mControlPanelSubsystem.matching();
        }
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        
        mControlPanelSubsystem.stopPanel();
        System.out.println("ControlPanelMatchColorCommand -- Ended");
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return mIsFinished;
    }

}