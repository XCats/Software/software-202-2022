package frc.robot.commands.controlPanel;

import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ControlPanelSubsystem;

/**
 * An example command that uses an example subsystem.
 */
public class ToggleControlPanelCommand extends CommandBase {
    @SuppressWarnings({ "PMD.UnusedPrivateField", "PMD.SingularField" })

    private final ControlPanelSubsystem mControlPanelSubsystem;
    private boolean mIsFinished;

    /**
     * Creates a new ExampleCommand.
     *
     * @param subsystem The subsystem used by this command.
     */
    public ToggleControlPanelCommand(ControlPanelSubsystem controlPanelSubsystem) {
        mControlPanelSubsystem = controlPanelSubsystem;
        // Use addRequirements() here to declare subsystem dependencies.
        addRequirements(mControlPanelSubsystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {

    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        mControlPanelSubsystem.toggle();
        mIsFinished = true;
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {

    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return mIsFinished;
    }
}
