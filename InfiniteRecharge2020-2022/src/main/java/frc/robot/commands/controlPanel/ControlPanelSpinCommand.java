package frc.robot.commands.controlPanel;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ControlPanelSubsystem;

public class ControlPanelSpinCommand extends CommandBase {

    private final ControlPanelSubsystem mControlPanelSubsystem;
    private boolean mIsFinished;

    public ControlPanelSpinCommand(ControlPanelSubsystem controlPanelSubsystem) {
        super();
        mControlPanelSubsystem = controlPanelSubsystem;
        addRequirements(mControlPanelSubsystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        System.out.println("ControlPanelSpinCommand -- Initialized");
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {

        if(mControlPanelSubsystem.getState()) {
            mControlPanelSubsystem.retract();
        }    
          
        mControlPanelSubsystem.spin();
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        mControlPanelSubsystem.stopPanel();       
        System.out.println("ControlPanelSpinCommand -- Ended");
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return mIsFinished;
    }

}