package frc.robot.commands.climber;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ClimbingSubsystem;

public class DeployClimberCommand extends CommandBase {

    public ClimbingSubsystem mClimbingSubsystem;
    private boolean isFinished = false;

    public DeployClimberCommand(ClimbingSubsystem climbingSubsystem) {
        this.mClimbingSubsystem = climbingSubsystem;
        super.addRequirements(climbingSubsystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        // Climb
        mClimbingSubsystem.toggle();
        isFinished = true;
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return isFinished;
    }
}